<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class UserController extends Controller
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function fileImportExport()
    {
        $user = DB::table('users')->select()->get();

        return view('file-import')->with('users',$user);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function fileImport(Request $request)
    {
        Excel::import(new UsersImport, $request->file('file')->store('temp'));
        return back();
    }

    public function show($id)
    {
        $user = DB::table('users')->select()->where('id', '=', $id)->first();
        $average_user_score = DB::table('calls')->select(DB::raw('avg(ext_call_score) as avg'))->where('user_id', '=', $id)->first();
        $calls = DB::table('calls')->select()->where('user_id', '=', $id)->orderBy('date')->limit(5)->get();
        return view('user.show')->with('user',$user)->with('average_user_score',$average_user_score)->with('calls',$calls);
    }

}
