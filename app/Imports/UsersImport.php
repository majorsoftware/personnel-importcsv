<?php

namespace App\Imports;

use App\Calls;
use App\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Validators\Failure;


class UsersImport implements ToCollection,WithHeadingRow,WithValidation, SkipsOnFailure
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            $users = DB::table('users')
                ->select()
                ->where('user', '=', $row['user'])
                ->first();
            if(empty($users)) {
                $user_id = User::create([
                    'user' => $row['user'],
                ]);
                $user_id=$user_id->id;
            }
            else{
                $user_id = $users->id;
            }
            $date = Carbon::parse($row['date']);

            Calls::create([
                'user_id' => $user_id,
                'client' => $row['client'],
                'client_type' => $row['client_type'],
                'date' => $date->format('Y-m-d h:i'),
                'duration' => $row['duration'],
                'type_of_call' => $row['type_of_call'],
                'ext_call_score' => $row['external_call_score']
            ]);

        }
    }
    public function rules(): array
    {
        return [
            '0' => 'unique:user',
        ];
    }



    /**
     * @param Failure[] $failures
     */
    public function onFailure(Failure ...$failures)
    {
        // TODO: Implement onFailure() method.
    }
}
