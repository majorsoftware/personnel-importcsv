@extends('layouts.home')
<button type="button" class="btn btn-primary"><a href="{{url('file-import-export')}}">Back</a></button>

<div class="container">
        <h1>Average User Score: {{ $average_user_score->avg }}</h1>
        <h1>User: {{ $user->user }}</h1>
    </div>


<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">Client</th>
        <th scope="col">Client Type</th>
        <th scope="col">Date</th>
        <th scope="col">Duration</th>
        <th scope="col">Type of Call</th>
        <th scope="col">External Call Score</th>
    </tr>
    </thead>
    <tbody>
    @foreach($calls as $call)
    <tr>
        <th scope="row">{{$call->client}}</th>
        <th scope="row">{{$call->client_type}}</th>
        <th scope="row">{{$call->date}}</th>
        <th scope="row">{{$call->duration}}</th>
        <th scope="row">{{$call->type_of_call}}</th>
        <th scope="row">{{$call->ext_call_score}}</th>
    </tr>
    @endforeach
    </tbody>
</table>
